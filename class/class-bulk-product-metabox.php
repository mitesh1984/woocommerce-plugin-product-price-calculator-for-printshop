<?php
/*
*   Bulk Product Metabox Class
*/
class WC_Product_BulkProductMetaBox{
    public function __construct() {
        add_action('add_meta_boxes',    array( $this,   '_wqp_meta_boxes' ) );
        add_action('save_post',         array( $this,   '_wqp_save_meta_data' ) );
        add_action('post_edit_form_tag',array( $this,   '_wqp_update_edit_form' ) );
    }
    
    public function _wqp_meta_boxes(){
        add_meta_box(
            'wp_wqp_attachment',
            'Download Attachment',
            array( $this, 'wp_wqp_attachment' ),
            'product',
            'side'
        );
    }
    
    public function wp_wqp_attachment( $post ){
        wp_nonce_field(plugin_basename(__FILE__), 'wqp_attachment_nonce');
        
        $wqp_pdf = get_post_meta( $post->ID, 'wqp_pdf', true );
     
        $html = '<p class="description">';
        if( $wqp_pdf && isset( $wqp_pdf['url'] ) ){
            $fileInfo = pathinfo( $wqp_pdf['file'] );
            $html .= '<a href="'.$wqp_pdf['url'].'" target="_blank">'.$fileInfo['filename'].'</a></br>';
            $html .= 'Replace your Attachment here.';
        }else{
            $html .= 'Upload your Attachment here.';
        }
        $html .= '</p>';
        $html .= '<input type="file" id="wqp_pdf" name="wqp_pdf" value="" size="25" />';
         
        echo $html;
    }
    
    function _wqp_save_meta_data($id) {
    /* --- security verification --- */
       if(!isset($_POST['wqp_attachment_nonce']) || !wp_verify_nonce($_POST['wqp_attachment_nonce'], plugin_basename(__FILE__))) {
         return $id;
       } // end if
          
       if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
         return $id;
       } // end if
          
       if('product' == $_POST['post_type']) {
         if(!current_user_can('edit_page', $id)) {
           return $id;
         } // end if
       } else {
           if(!current_user_can('edit_page', $id)) {
               return $id;
           } // end if
       } // end if
       /* - end security verification - */
        
       // Make sure the file array isn't empty
       if(!empty($_FILES['wqp_pdf']['name'])) {
            
           // Setup the array of supported file types. In this case, it's just PDF.
           $supported_types = array('application/zip');
            
           // Get the file type of the upload
           $arr_file_type = wp_check_filetype(basename($_FILES['wqp_pdf']['name']));
           $uploaded_type = $arr_file_type['type'];
            
           // Check if the type is supported. If not, throw an error.
           if(in_array($uploaded_type, $supported_types)) {
    
               // Use the WordPress API to upload the file
               $upload = wp_upload_bits($_FILES['wqp_pdf']['name'], null, file_get_contents($_FILES['wqp_pdf']['tmp_name']));
        
               if(isset($upload['error']) && $upload['error'] != 0) {
                   wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
               } else {
                   add_post_meta($id, 'wqp_pdf', $upload);
                   update_post_meta($id, 'wqp_pdf', $upload);     
               } // end if/else
    
           } else {
               wp_die("The file type that you've uploaded is not a ZIP.");
           } // end if/else
            
       } // end if
            
    }
    
    function _wqp_update_edit_form() {
        echo ' enctype="multipart/form-data"';
    } // end update_edit_form
            
}
new WC_Product_BulkProductMetaBox();
