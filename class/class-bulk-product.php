<?php
/*
*   Bulk Product Class
*/
class WC_Product_BulkProduct extends WC_Product {
    public function __construct( $product ) {
        $this->product_type = 'bulk_product';
        parent::__construct( $product );
    }
}
