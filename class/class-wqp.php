<?php
/*
*   Main plugin class
*/

class WQP{
    
    /*
     *  Handle all woocommerce/site request 
     */
    public function __construct(){
        if( is_admin() ){
            add_action( 'admin_enqueue_scripts',    array( $this , '_wqp_admin_script' ) );
        }else{
            add_action("wp_enqueue_scripts",        array( $this,  '_wqp_script' ) ); 
        }
		//Backend Handler & Filters
		add_action( 'init',							array( $this, 'add_attributes' ) ); // Add product attributes
        add_action( 'plugins_loaded',               array( $this, 'register_bulk_product_type' ) ); // Register the bulk product type after init
		add_action( 'admin_footer', 				array( $this,	'bulk_product_custom_js' ) ); // Show/add price fields for bulk products 
		add_action( 'woocommerce_product_options_general_product_data', array( $this ,'_wqp_qunatity_fields' ) , 1 ); // Add price quantity wise to product
		add_action( 'save_post',					array( $this, '_wqp_save_post' ), 99 ); // Save untuwisr price data
		add_action( 'admin_init',					array( $this, '_wqp_import_product'), 99 );
	
        add_filter( 'product_type_selector',        array( $this, 'add_bulk_product' ) ); // Add to product type dropdown
		
		//Frontend Handlers & Filters
		add_action( 'woocommerce_before_single_product_summary',array( $this, '_wqp_remove_product_price' ) ); // Handle loading actions & filters
		add_action( 'woocommerce_after_single_product_summary',	array( $this, '_wqp_product_summary' ) ); // Handle bulk product summary
		add_action( 'wp_footer', 					array( $this,	'bulk_product_custom_js' ) ); // Show/add price fields for bulk products
		add_action( 'woocommerce_add_to_cart',		array( $this, '_wqp_add_to_cart' ), 10, 6 ); // Handle add to cart bulk product
		add_action( 'woocommerce_before_cart_table',array( $this, '_wqp_remove_qty_cart_table' ) ); // Remove qty input box from cart page
		add_action( 'woocommerce_after_cart_table', array( $this, '_wqp_add_qty_cart_table' ) ); // Add qty input box from cart page
		add_action( 'woocommerce_remove_cart_item',	array( $this, '_wqp_remove_product_attribute' ), 10, 2 ); // Remove product attribute on product remove from cart
		add_action( 'woocommerce_add_order_item_meta',				array( $this, '_wqp_add_product_attribute' ), 10, 3 ); // Add bulk product attibute on order processing
		add_action( 'woocommerce_order_details_after_order_table',	array( $this, '_wqp_order_item_attribute' ) ); // Display purchased bulk product attibutes
		add_action( 'woocommerce_email_customer_details',		array( $this, '_wqp_email_order_item_attribute' ), 10, 3 ); // Display purchased bulk product attibutes on email
		add_action( 'woocommerce_before_order_itemmeta',	array( $this, '_wqp_order_item_detail' ), 10, 3 );  // Display purchased bulk product attibutes on backend
		add_action( 'woocommerce_checkout_order_processed', array( $this, '_wqp_clean_product_session' ) ); // Clean session for product attribut on order placed
		add_action( 'woocommerce_product_option_terms',		array( $this, '_wqp_product_option_terms' ), 10, 2 ); // Add bulk price option to product attribute
		add_action( 'manage_product_posts_custom_column', 	array( $this, '_wqp_shortcode_in_listing' ), 10, 2); // Add shortcode in product listing page
		add_action( 'woocommerce_product_thumbnails',	array( $this, '_wqp_download_pdf' ), 30 ); // Add download PDF under product images	
		
		add_filter( 'wc_get_template',				array( $this, '_wqp_get_template' ), 10, 5 ); // Handle simple & bulk product add to cart template
		add_filter( 'woocommerce_is_purchasable',	array( $this, '_wqp_purchasable' ), 10, 2 ); // Make product purchable for bulk product type
		add_filter( 'woocommerce_get_price', 		array( $this, '_wqp_price' ), 10, 2 ); // Handle bulk product price
		add_filter( 'woocommerce_checkout_cart_item_quantity', 	array( $this, '_wqp_checkout_cart_item_quantity' ), 10, 3 ); // Filter quantity in checkout page
		add_filter( 'woocommerce_order_item_quantity_html',		array( $this, '_wqp_order_item_quantity_html'), 10, 2 ); // Filter quantity for order view page
		add_filter( 'woocommerce_email_order_item_quantity',	array( $this, '_wqp_order_item_quantity_html' ), 10, 2 ); // Filter quantity for order email page
		add_filter( 'manage_edit-product_columns', 	array( $this, '_wqp_add_shortcode_column' ) ); // Add shortcode column for product page listing.
		
		// Shortcode
		add_shortcode( 'pricing_calculator',	array( $this, '_wqp_pricing_calculator' ) ); // Add shortcode for product pricing.
		
    }
    
    // Handle front end script/css
    public function _wqp_script(){
        wp_register_style( 'wqp-style',     plugins_url('css/style.css',dirname(__FILE__)));
        wp_register_script( 'wqp-selectbox',   plugins_url('js/jquery.selectbox-0.2.js',dirname(__FILE__)),array(),'1.0.0',true );
        wp_register_script( 'wqp-script',   plugins_url('js/custom.js',dirname(__FILE__)),array(),'1.0.0',true );
		
		
		wp_enqueue_style('wqp-style');
		wp_enqueue_script('wqp-selectbox');
		wp_enqueue_script('wqp-script');
    }  

    // Handle back end script/css
    public function _wqp_admin_script(){
        wp_register_style( 'wqp-style' ,    plugins_url('css/style_admin.css',dirname(__FILE__) ) );
        wp_register_script( 'wqp-script',   plugins_url('js/custom_admin.js',dirname(__FILE__)),array('jquery'),'1.0.0',true );
    }

    // Register the bulk product type after init
    public function register_bulk_product_type(){
        require_once(WQP_PATH ."/class/class-bulk-product.php");
    }
    
    // Add to product type dropdown
    public function add_bulk_product( $types ){
        // Key should be exactly the same as in the class
        $types[ 'bulk_product' ] = __( 'Bulk Product' );
        return $types;
    }
    
	// Add price quantity wise to product	
	public function _wqp_qunatity_fields(){;
		$this->get_template('admin/bulk-product-metabox.php');
	}
	
	// Show/add price fields for bulk products
	public function bulk_product_custom_js() {
		if ( 'product' != get_post_type() ) :
			return;
		endif;
		wp_enqueue_style('wqp-style');
		wp_enqueue_script('wqp-script');
		
	}
	
	// Save unitwise price data
	public function _wqp_save_post( $post_id ){
		
		if ( 'product' != get_post_type() || wp_is_post_revision( $post_id ) ) :
			return;
		endif;
		
		$product_units = $_POST['product_units'];
		if( isset( $product_units ) ){
			update_post_meta( $post_id, '_units', array_unique( $product_units ) );
		}else{
			delete_post_meta( $post_id, '_unitS' );
		}
		
		$product_custom_text = $_POST['product_custom_text'];
		if( isset( $product_custom_text ) ){
			update_post_meta( $post_id, '_custom_text', array_unique( $product_custom_text ) );
		}else{
			delete_post_meta( $post_id, '_custom_text' );
		}
		
		$_product_attributes = get_post_meta( $post_id, '_product_attributes', true );
		$_post_data = array_keys( $_POST['attribute_bulk'] );
		foreach( $_product_attributes as $key => $attributes ){
			if( in_array( $key, $_post_data )  ){
				$_product_attributes[$key]['is_bulk'] = 1;
			}else{
				$_product_attributes[$key]['is_bulk'] = 0;
			}
		}
		update_post_meta( $post_id, '_product_attributes', $_product_attributes );
	}
	
	// Handle loading actions & filters
	public function _wqp_remove_product_price(){
		if( $this->is_bulk_product() ){
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);	
		}
	}
	
	// Handle simple & bulk product add to cart template
	public function _wqp_get_template( $located, $template_name, $args, $template_path, $default_path ){
		if( $template_name == 'single-product/add-to-cart/simple.php' ){
			if( $this->is_bulk_product() ){
				$located = WQP_PATH.'/templates/'.$template_name;	
			}
		}
		return $located;
	}
	
	// Check product is purchable or not for bulk product type
	public function _wqp_purchasable( $purchasable, $product ){
		if( $this->is_bulk_product( $product ) ){
			return true;
		}
		return $purchasable;
	}
	
	// Handle bulk product summary
	public function _wqp_product_summary(){
		if( $this->is_bulk_product() ){
			add_filter( 'woocommerce_get_product_attributes', array( $this, '_wqp_product_attribute' ) );
		}
	}
	
	// Remove product attributes from detail section
	public function _wqp_product_attribute(){
		return array();
	}
	
	// Add template for bulk product
	public function get_template( $template_name ){
		if( file_exists( WQP_PATH.'/templates/'.$template_name ) ){
			require( WQP_PATH.'/templates/'.$template_name );
		}
	}
	
	// Handle bulk product purchase
	public function _wqp_price( $price, $product ){
		global $cart_price;
		$_unit_price = get_post_meta( $product->id, '_unit_price', true );
		if( is_array( $_unit_price ) && count( $_unit_price ) > 0 && !is_admin() ){
			$cart_products = WC()->session->get( 'cart' );
			$product_attribute = WC()->session->get( 'wqp_product_attr');
			if( count( $cart_products ) > 0 ){
				foreach( $cart_products as $cart_content ){
					if( $cart_content['product_id'] == $product->id ){
						$price = $_unit_price[ $product_attribute[$cart_content['product_id']]['product-id'] ];
					}
				}
			}
			if( empty( $price ) ){
				//$price = array_values($_unit_price)[0];
			}
		}
		return $price;
	}
	
	// Check bulk product is or not
	public function is_bulk_product( $objProduct = null ){
		global $product;
		if( !$objProduct ){
			$objProduct = $product;
		}
		if( $objProduct ){
			$_unit_price = get_post_meta( $objProduct->id, '_unit_price', true );
			if( is_array( $_unit_price ) && count( $_unit_price ) > 0 ){
				return true;
			}
			return false;
		}
		return false;
	}
	
	// Remove previous added bulk product and replace with new one
	public function _wqp_add_to_cart( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ){
		$_unit_price = get_post_meta( $product_id, '_unit_price', true );
		if( is_array( $_unit_price ) && count( $_unit_price ) > 0 ){
			unset( $_POST['add-to-cart'] );
			$product_attribute[ $product_id ] = $_POST;
			WC()->session->set( 'wqp_product_attr', $product_attribute );
			WC()->cart->cart_contents[$cart_item_key]['quantity'] = $quantity;
		}
	}
	
	// Handle quatity input box in cart page
	public function _wqp_remove_qty_cart_table(){
		add_filter( 'woocommerce_cart_item_quantity', array( $this, '_wqp_quantity_input_for_cart' ), 10, 3 );
	}
	
	// Handle quatity input box in cart page
	public function _wqp_add_qty_cart_table(){
		remove_filter( 'woocommerce_cart_item_quantity', array( $this, '_wqp_quantity_input_for_cart' ), 10, 3 );
	}
	
	// Remove quantity input box frm cart page for bulk product
	public function _wqp_quantity_input_for_cart( $product_quantity, $cart_item_key, $cart_item ){
		$_unit_price = get_post_meta( $cart_item['product_id'], '_unit_price', true );
		if( is_array( $_unit_price ) && count( $_unit_price ) > 0 ){
			$product_attribute = WC()->session->get( 'wqp_product_attr');
			$product_attr_id = explode('-', $product_attribute[ $cart_item['product_id'] ]['product-id'] );
			return array_reverse( $product_attr_id )[0] . ' Units';
		}
		return $product_quantity;
	}
	
	// Filter quantity from checkout page
	public function _wqp_checkout_cart_item_quantity( $product_quantity, $cart_item, $cart_item_key ){
		$_unit_price = get_post_meta( $cart_item['product_id'], '_unit_price', true );
		if( is_array( $_unit_price ) && count( $_unit_price ) > 0 ){
			$product_attribute = WC()->session->get( 'wqp_product_attr');
			$product_attr_id = explode('-', $product_attribute[ $cart_item['product_id'] ]['product-id'] );
			return '<strong class="product-quantity">' . sprintf( '&times; %s Units', array_reverse( $product_attr_id )[0] ) . '</strong>';;
		}
		return $product_quantity;
	}
	
	// Filter quntity for order detail page
	public function _wqp_order_item_quantity_html( $product_quantity, $cart_item){
		$_unit_price = get_post_meta( $cart_item['product_id'], '_unit_price', true );
		if( is_array( $_unit_price ) && count( $_unit_price ) > 0 && isset( $product_attribute['product-id'] ) ){
			$product_attribute = unserialize($cart_item['item_meta']['_wqp_attr'][0]);
			$product_attr_id = explode('-', $product_attribute['product-id'] );
			return ' <strong class="product-quantity">' . sprintf( '&times; %s Units', array_reverse( $product_attr_id )[0] ) . '</strong>';
		}
		return $product_quantity;
	}
	
	// Remove product attribute on remove products from cart
	public function _wqp_remove_product_attribute( $cart_item_key, $obj ){
		$product_attribute = WC()->session->get( 'wqp_product_attr');
		unset( $product_attribute[ WC()->cart->cart_contents[$cart_item_key]['product_id'] ] );
		WC()->session->set( 'wqp_product_attr', $product_attribute );
	}
	
	// Add bulk product attribute on orcer completeion
	public function _wqp_add_product_attribute( $item_id, $values, $cart_item_key ){
		$product_attribute = WC()->session->get( 'wqp_product_attr');
		if( isset( $product_attribute[ $values['product_id'] ] ) ){
			wc_add_order_item_meta( $item_id, '_wqp_attr', $product_attribute[ $values['product_id'] ] );
		}
	}
	
	// Display bulk product attributes on order detail page.
	public function _wqp_order_item_attribute( $order ){
		global $order_detail;
		$order_detail = $order;
		$this->_wqp_upload_art();
		$this->get_template( 'order/order-attributes.php' );
	}
	
	// Upload art after order process complete from front end
	public function _wqp_upload_art(){
		if( !isset( $_POST['upload_art'] ) ){
			return;
		}
		$upload_dir = wp_upload_dir();
		$target_dir = $upload_dir['basedir'].'/art/';
		$target_url = $upload_dir['baseurl'].'/art/';
		foreach( $_FILES['art']['name'] as $file_id => $fileLists ){
			if( !is_dir( $target_dir ) ){
				mkdir( $target_dir );
			}
			$artFiles = wc_get_order_item_meta( $file_id, 'art' );
			foreach( $fileLists as $key => $fileName ){
				
				if( $_FILES["art"]["name"][$file_id][$key] == '' ){
					continue;
				}
				
				$target_dir = $upload_dir['basedir'].'/art/';
				$target_url = $upload_dir['baseurl'].'/art/';
				$imageFileType = pathinfo($fileName,PATHINFO_EXTENSION);
				$name = md5(rand(11111, 99999));
				$target_file = $target_dir . basename( $file_id ) .'-' . $name .'.'.$imageFileType;
				$target_url = $target_url . basename( $file_id ).'-' . $name.'.'.$imageFileType;
				$uploadOk = 1;
				
				// Check if image file is a actual image or fake image
				$check = getimagesize($_FILES["art"]["tmp_name"][$file_id][$key]);
				if($check !== false) {
					$uploadOk = 1;
				} else {
					echo $fileName." is not an image.<br />";
					$uploadOk = 0;
				}
				
				// Check if file already exists
				if (file_exists($target_file)) {
					echo "Sorry, $fileName already exists.<br />";
					$uploadOk = 0;
				}
				// Check file size
				if ($_FILES["art"]["size"][$file_id][$key] > 500000) {
					echo "Sorry, your $fileName is too large.<br />";
					$uploadOk = 0;
				}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
					echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.<br />";
					$uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					echo "Sorry, your $fileName was not uploaded.<br />";
				// if everything is ok, try to upload file
				} else {
					if (move_uploaded_file($_FILES["art"]["tmp_name"][$file_id][$key], $target_file)) {
						$artFiles[$key] =  $target_url;
						echo "The file ". basename( $_FILES["art"]["name"][$file_id][$key]). " has been uploaded.<br />";
					} else {
						echo "Sorry, there was an error uploading your file $fileName.<br />";
					}
				}
			}
			if( count( $artFiles ) > 0 ){
				wc_update_order_item_meta( $file_id, 'art', $artFiles );
			}
		}
	}
	
	// Display bulk product attributes on email order page.
	public function _wqp_email_order_item_attribute($order, $sent_to_admin = false, $plain_text = false ){
		global $order_detail;
		$order_detail = $order;
		$this->get_template( 'order/email-order-attributes.php' );
	}
	
	// Display bulk product attibutes on order detail in backend
	public function _wqp_order_item_detail ( $item_id, $item, $_product ){
			global $item_detail;
			wp_enqueue_style('wqp-style');
			$item_detail = $item;
			$this->get_template( 'order/html-order-item-attribute.php' );
	}

	// Clean product attributes after order compeltion
	public function _wqp_clean_product_session( $order ){
		WC()->session->set( 'wqp_product_attr', array() );
	}
	
	// Add bulk price option to product attribute
	public function _wqp_product_option_terms( $attribute_taxonomy, $i ){
		$_product_attributes = array();
	
		if( isset( $_REQUEST['post'] ) ){
			$_product_attributes = get_post_meta( $_REQUEST['post'], '_product_attributes', true );
		}
		woocommerce_form_field( 'attribute_bulk[pa_'.$attribute_taxonomy->attribute_name.']', array(
        'type'          => 'checkbox',
        'class'         => array('wqp-checkbox', 'show_if_bulk_product'),
		'input_class'	=> array('checkbox'),
        'label'         => __(' Used for bulk price'),
		'default'		=> 1,
        ), isset( $_product_attributes['pa_'.$attribute_taxonomy->attribute_name]['is_bulk'] ) ? $_product_attributes['pa_'.$attribute_taxonomy->attribute_name]['is_bulk'] : 1 );
	}
	
	//Shortcode for product pricing
	public function _wqp_pricing_calculator( $atts ){
		global $wqp_product_id;
		$atts = shortcode_atts( array(
			'product_id' => ''
		), $atts );
		
		wp_enqueue_script('wqp-selectbox');
		wp_enqueue_script('wqp-script');
		
		$wqp_product_id = $atts['product_id'];
		ob_start();
		$this->get_template('shortcode/pricing-calculator.php');
		$content = ob_get_contents();
		ob_end_clean();
		echo $content;
	}

	// Add shorcode column in product listing page		
	public function _wqp_add_shortcode_column( $columns ) {
		$newColumn = array();
		foreach( $columns as $key => $column ){
			if( $key == 'product_tag' ){
				unset($columns["product_tag"]);
				$newColumn["pricing-shortcode"] = "Pricing Shortcode";		
			}else{
				$newColumn[$key] = $column;
			}
		}
		return $newColumn;
	}

	//Add shortcode data in product listing page	
	function _wqp_shortcode_in_listing( $colname, $cptid ) {
		 if ( $colname == 'pricing-shortcode'){
			  echo '[pricing_calculator product_id="'.$cptid.'"]';
		 }
	}
	
	// Add download PDF link under product images.
	public function _wqp_download_pdf(){
		global $product;
		$wqp_pdf = get_post_meta( $product->id, 'wqp_pdf', true );
		if( $wqp_pdf && isset( $wqp_pdf['url'] ) ){
			$fileInfo = pathinfo( $wqp_pdf['file'] );
			echo '</br><a href="'.$wqp_pdf['url'].'" target="_blank" class="button">'.__('Download').'</a></br>';
		}
	}
	
	//Import Porducts
	public function _wqp_import_product(){
		//return;
		$_wqp_imports = get_option( '_wqp_imports', 0 );
		if( !$_wqp_imports ){
			
			if( !class_exists('WP_Import') ){
				require_once( WQP_PATH .'plugins/wordpress-importer/wordpress-importer.php' );
			}
			add_action( 'wp_import_insert_post' , array( $this, '_wqp_import_attachments' ) );
			
			$objWP_Import = new WP_Import();
			set_time_limit(0);
			$objWP_Import->fetch_attachments = true;
			$objWP_Import->import( WQP_PATH .'data/products.xml');
			update_option( '_wqp_imports', 1 );
		}
	}
	
	// Add product attributes
	public function add_attributes(){
		global $wpdb;
		
		$_wqp_imports = get_option( '_wqp_imports', 0 );
		if( !$_wqp_imports ){
			$product_attributes = array( 'Coating', 'Color', 'Finishing', 'Grommets', 'Paper', 'Pocket', 'Round Corners', 'Shrink Wrapping', 'Size' );
			
			foreach( $product_attributes as $product_attribute ){
				$slug = str_replace(' ','-', strtolower($product_attribute) );
					
				if( !taxonomy_exists( 'pa_'.$slug ) ){
					$attribute = array(
						'attribute_label'   => $product_attribute,
						'attribute_name'    => $slug,
						'attribute_type'    => 'select',
						'attribute_orderby' => '',
						'attribute_public'  => 0
					);
					
					$wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );
					
					flush_rewrite_rules();
					delete_transient( 'wc_attribute_taxonomies' );
				
					do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
				
					register_taxonomy(
						'pa_'.$slug,
						'product',
						array(
							'label' => __( $product_attribute ),
							'rewrite' => array( 'slug' => $slug ),
							'hierarchical' => true,
						)
					);
				}
			}
		}
	}
	
	//Import Attachments
	public function _wqp_import_attachments( $parent_post_id ){
		
		$post = get_post($parent_post_id);
		
		$upload_dir = wp_upload_dir();
		
		$imageName = $post->post_name;
		
		$orgFileName = WQP_PATH.'data/attachments/'.$imageName.'.jpg';
		
		$filename = $upload_dir['path'].'/'.$imageName.'.jpg';
		
		if( copy( $orgFileName, $filename ) ){
			
			// Check the type of file. We'll use this as the 'post_mime_type'.
			$filetype = wp_check_filetype( basename( $filename ), null );
			
			// Get the path to the upload directory.
			$wp_upload_dir = wp_upload_dir();
			
			// Prepare an array of post data for the attachment.
			$attachment = array(
				'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
				'post_mime_type' => $filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);
			
			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );
			
			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			
			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			
			set_post_thumbnail( $parent_post_id, $attach_id );
			update_post_meta( $parent_post_id, '_thumbnail_id', $attach_id );
		}else{
			wp_delete_post( $parent_post_id, true );
		}
		
		/**
		 *	PDF Attachment
		 **/
		
		$orgFileName = WQP_PATH.'data/downloads/'.$imageName.'.zip';
		
		$filename = $upload_dir['path'].'/'.$imageName.'.zip';
		
		if( file_exists($orgFileName) ){
			
			$upload = wp_upload_bits($imageName.'.zip', null, file_get_contents($orgFileName));
        
			if(isset($upload['error']) && $upload['error'] != 0) {
				wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
			} else {
				add_post_meta($parent_post_id, 'wqp_pdf', $upload);
				update_post_meta($parent_post_id, 'wqp_pdf', $upload);     
			} // end if/else
		}
		//die();
		
	}
}

$objWQP = new WQP();
