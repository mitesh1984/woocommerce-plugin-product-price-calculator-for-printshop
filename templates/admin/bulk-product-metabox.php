<?php

global $woocommerce, $post;
$_units = get_post_meta( $post->ID, '_units', true );
$_custom_text = get_post_meta( $post->ID, '_custom_text', true );
$bulk_product_price = false;
$product = new WC_Product_Simple( $post->ID ); 
$product_attributes = $product->get_attributes();
foreach ( $product_attributes as $attribute ) :
	if ( $attribute['is_taxonomy'] && isset( $attribute['is_bulk'] ) ) {
		$bulk_product_price = true;
	}
endforeach;
?>
<div class="options_group show_if_bulk_product">
	<h4 style="padding-left:10px;"><?php echo __('Unitwise Price','wqp'); ?></h4>
	
<?php

if( is_array( $_units ) && count( $_units ) > 0 && $bulk_product_price ){
	?>
	<div id="message" class="inline notice woocommerce-message">
		<p>You can add a price variation from here.</p>
		<p>
			<a class="button-primary" href="<?php echo plugins_url('admin/bulk-product-price.php?product_id='.$_GET['post'] ,dirname(__FILE__) ); ?>">Add Price</a>
		</p>
	</div>
	<?php
}else{
	?>
	<div id="message" class="inline notice woocommerce-message">
		<p>Before you can add a price you need to add some variation attributes on the <strong>Attributes</strong> tab.</p>
	</div>
	<?php
}
?>
<div class="bulk_product_handler_panel">
	<?php
		if( is_array( $_units ) && count( $_units ) > 0 ){
			foreach( $_units as $unit ){
				?>
		<div class="unit_price_panel">
						<input type="text" class="product_units" name="product_units[]" value="<?php echo $unit; ?>" />
						<input type="button" class="product_unit_remove button" value="Remove" />
						</div>
		<?php
			}
		}
		?>
</div>
<input type="button" class="product_unit_add button" value="Add Unit" />
<hr>
<div class="bulk_product_text_panel">
	<?php
		if( is_array( $_custom_text ) && count( $_custom_text ) > 0 ){
			foreach( $_custom_text as $_custom ){
				?>
				<div class="unit_text_panel">
					<input type="text" class="product_texts" placeholder="Add lable for input text" name="product_custom_text[]" value="<?php echo $_custom; ?>" />
					<input type="button" class="product_text_remove button" value="Remove" />
				</div>
		<?php
			}
		}
		?>
</div>
<input type="button" class="product_custom_text_add button" value="Add Custom Text Box" />
</div>