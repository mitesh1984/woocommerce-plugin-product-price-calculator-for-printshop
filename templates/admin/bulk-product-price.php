<?php
require_once( '../../../../../wp-load.php' );

if( !is_admin() ){
	//return;
}
$product_id = $_GET['product_id'];
$updateFlag = false;
$totalAttr = array();

$product = new WC_Product_Simple( $product_id ); 
$attributes = $product->get_attributes();

if( isset( $_POST['save_prices'] ) ){
	
	foreach( $attributes as $attribute ){
		if( !$attribute['is_bulk']){
			$totalAttr[] = $attribute['name'];
		}
	}
	$finalPriceList = array();
	foreach( $_POST['price'] as $key => $unit ){
		$subKey = substr( $key, 0, strpos( $key, '-pa' ));
		$keyWithoutUnit = $subKey;
		$_single_unit = array_reverse( explode( '-', $key ) )[0];
		foreach( $totalAttr as $attribute ){
			$keyWithoutUnit .= '-'.$attribute;
			$unit += floatval( $_POST['extra-price'][$subKey.'-'.$attribute] ) * $_single_unit;
		}
		$keyWithoutUnit .= '-'.$_single_unit;
		$finalPriceList[ $keyWithoutUnit ] = $unit;
	}
	//$finalPriceList
	update_post_meta( $product_id, '_unit_price', $finalPriceList );
	update_post_meta( $product_id, '_single_unit_price', $_POST['price'] );
	update_post_meta( $product_id, '_unit_extra_price', $_POST['extra-price'] );
	$updateFlag = true;
}

$product_units = get_post_meta( $product_id, '_units', true );
$product_unit_price = get_post_meta( $product_id, '_single_unit_price', true );
$product_unit_extra_price = get_post_meta( $product_id, '_unit_extra_price', true );

$factValue = 1;
$totalAttr = array();
$termArray = array();

?>
<html>
	<head>
		<title><?php echo get_the_title($product_id); ?> Price List</title>
		<link type="text/css" rel="stylesheet" media="all" href="<?php echo WQP_URL?>css/style_admin.css" />
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script>
			(function($){
				$(document).ready(function(){
					
				var $table = $('.bulk_price_list_panel table'),
					$bodyCells = $table.find('tbody tr:first').children(),
					colWidth;
				
				$table.addClass('scroll');
				
				$('.bulk_price_list .prefill-values').click(function(){
					var currentRow = jQuery(this).parents('tr').index() + 1;
					var parentRow = currentRow - 1;
					$('.bulk_price_list table tr:nth-child('+parentRow+') input[type="text"]').each(function(){
					  columnNumber = $(this).parent('td').index() + 1;
					  $('.bulk_price_list table tbody tr:nth-child('+currentRow+') td:nth-child('+columnNumber+') input[type="text"]').val( $(this).val() );
				  });
				});


				
				/*$('.bulk_price_list input[type="text"]').keyup(function(){
					var val = $(this).val();
					if(isNaN(val)){
						 val = val.replace(/[^0-9\.]/g,'');
						 if(val.split('.').length>2) 
							 val =val.replace(/\.+$/,"");
					}
					$(this).val(val); 
				});*/
				// Adjust the width of thead cells when window resizes
				$(window).resize(function () {
				
					// Get the tbody columns width array
					colWidth = $bodyCells.map(function () {
						return $(this).width();
					}).get();
				
					// Set the width of thead columns
					$table.find('thead tr').children().each(function (i, v) {
						$(v).width(colWidth[i]);
					});
				
				}).resize(); // Trigger resize handler
				
				
					// Get the tbody columns width array
					colWidth = $bodyCells.map(function () {
						return $(this).width();
					}).get();
				
					// Set the width of thead columns
					$table.find('thead tr').children().each(function (i, v) {
						$(v).width(colWidth[i]);
					});
					
				});
			})(jQuery);
		</script>
	</head>
	<body class="bulk_price_list_panel">
		<div class="bulk_price_list">
			<form method="post" action="">
				<div class="price_header">
					<a class="back_link" href="<?php echo admin_url('/post.php?post='.$product_id.'&action=edit'); ?>">Back to product</a>
					<h2><?php echo get_the_title($product_id); ?> Price List</h2>
					<input class="submit" type="submit" name="save_prices" value="Save Prices" />
					<?php
					if( $updateFlag ){
						echo '<p>Price is updated!</p>';	
					}
					?>
				</div>
				<table class="scroll">
				<thead>
					<tr>
			<?php
					$fixedPriceAttr = '';
					$fixedPriceList = array();
					
					foreach ( $attributes as $attribute ) :
					?>
						<?php
							if ( $attribute['is_taxonomy'] ) {
								if( $attribute['is_bulk'] ){
									$termID = array();
									?>
									<th><?php echo wc_attribute_label( $attribute['name'] ); ?></th>
									<?php
									$values = wp_get_post_terms( $product_id, $attribute['name'], array( 'fields' => 'all' ) );
									
									foreach( $values as $value ){
										$termArray[ $value->term_id ] = $value->name;
										$termID[] = $value->term_id;
									}
									$totalAttr[] = $termID;
								}else{
									$fixedPriceAttr .= '<th>'.wc_attribute_label( $attribute['name'] ).'</th>';
									$fixedPriceList[] = $attribute['name'];
								}
							}
							
					endforeach;
					
					echo $fixedPriceAttr;
					
					foreach($product_units as $unit){
						?>
						<th>Units: <?php echo $unit; ?></th>
						<?php
					}
					?>
					<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
				
				
				$combos = _wqp_combination( $totalAttr );
				$firstRow = true;
				foreach( $combos as $newRow ){
					?>
					<tr>
					<?php
					$inputName = '';
					foreach( $newRow as $newTD ){
						?>
						<td><?php echo $termArray[ $newTD ]; ?></td>
						<?php
						$inputName .= $newTD.'-';
					}
					
					$nameForFixedPriceList = $inputName;
					foreach( $fixedPriceList as $fixedPrice ){
						$inputName .= $fixedPrice.'-';
						$name = $nameForFixedPriceList.$fixedPrice;
						$price = isset( $product_unit_extra_price[ $name ] ) ? $product_unit_extra_price[ $name ] : 0;
						?>
						<td><input type="text" name="extra-price[<?php echo $name; ?>]" value="<?php echo $price; ?>" /></td>
						<?php
					}
					
					foreach($product_units as $unit){
						$name = $inputName.$unit;
						$price = isset( $product_unit_price[ $name ] ) ? $product_unit_price[ $name ] : 0;
						?>
						<td><input type="text" name="price[<?php echo $name; ?>]" value="<?php echo $price; ?>" /></td>
						<?php
					}
					?>
						<td><input type="button" <?php echo ($firstRow ? 'disabled' : '')?> class="prefill-values" value="Pre Fill" /></td>
					</tr>
					<?php
					if( $firstRow ){
						$firstRow = false;
					}
				}
				?>
					</tbody>
				</table>
				
			</form>
		</div>
	</body>
</html>