<?php
global $wqp_product_id;

$product = new WC_Product_Simple( $wqp_product_id );

$attributes = $product->get_attributes();
$_unit_price = get_post_meta( $wqp_product_id, '_unit_price', true );
$_units = get_post_meta( $wqp_product_id, '_units', true );


if( is_array( $_unit_price ) && count( $_unit_price ) > 0 ){
	foreach( $_unit_price as $key => $unit ){
		$_single_unit = array_reverse( explode( '-', $key ) )[0];
		$_unit_price[ $key ] = '<div class="total"><div><h4>printing total</h4></div><div>'.wc_price( $unit ).'</div></div><div class="total"><div><h4>unit price</h4></div><div>'.wc_price( $unit / $_single_unit ).'</div></div>';
	}
}

$product_attr = WC()->session->get( 'wqp_product_attr' );
if( isset( $product_attr[ $product->id ] ) ){
	$product_attr = $product_attr[ $product->id ];
}

?>
<div class="price_calculator_panel">
<form action="" method="post">
    <div class="price-calculator bulk_product_panel">             
            	<div class="calculator-title text-center">
	            	<h2>Pricing calculator</h2>
                </div>
                <div class="business-cards text-center">
                	<h1><?php echo get_the_title( $wqp_product_id ) ?></h1>
                    <div class="sections">
						<?php
						foreach ( $attributes as $attribute ) :
							if( $attribute['is_bulk'] ):
							?>
							<div class="card-options">
								<h6><?php echo wc_attribute_label( $attribute['name'] ); ?></t6>
								<div class="select-div"><?php
									if ( $attribute['is_taxonomy'] ) {
										$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'all' ) );
										?>
										<select name="<?php echo $attribute['name']?>">
										<?php
										foreach( $values as $value ){
											?>
											<option data-id="<?php echo $value->term_id;?>" <?php echo ( isset( $product_attr[ $attribute['name'] ] ) && $product_attr[ $attribute['name'] ] == $value->name ? 'selected' : '' )?> value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>
											<?php
										}
										?>
										</select>
										<?php
									} 
								?></div>
							</div>
						<?php
							endif;
						endforeach;
						foreach ( $attributes as $attribute ) :
							if( !$attribute['is_bulk'] ):
							?>
							<div class="card-options">
								<h6><?php echo wc_attribute_label( $attribute['name'] ); ?></t6>
								<div class="select-div"><?php
									if ( $attribute['is_taxonomy'] ) {
										$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'all' ) );
										?>
										<select name="<?php echo $attribute['name']?>">
										<?php
										foreach( $values as $value ){
											?>
											<option data-id="<?php echo $attribute['name'];?>" <?php echo ( isset( $product_attr[ $attribute['name'] ] ) && $product_attr[ $attribute['name'] ] == $value->name ? 'selected' : '' )?> value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>
											<?php
										}
										?>
										</select>
										<?php
									} 
								?></div>
							</div>
						<?php
							endif;
						endforeach;
						?>
                    </div>
                    <div class="sections">
                        <div class="price-title text-left">
                            <h5>quantity</h5>
                        </div>
                        <div class="card-options">                    	
                            <div class="select-div">
								
								<select name="units" data-price='<?php echo json_encode( $_unit_price ); ?>'>
								<?php
								if( is_array( $_units ) && count( $_units ) > 0 ){
									$product_units = 0;
									if( isset( $product_attr[ 'product-id' ] ) ) {
										$product_units = array_reverse( explode('-', $product_attr[ 'product-id' ] ) )[0];	
									}
									
									foreach( $_units as $key => $unit ){
										echo '<option data-id="'.$unit.'" '.( $product_units == $unit ? 'selected' : '' ).' value="'.$unit.'">'.$unit.'</option>';
									}
								}
									?>
								</select>
                            </div>
                        </div>
                        <!--div class="card-options">
                            <h6>versions</h6>
                            <input type="text" name="versions" id="versions" value="1" />
                        </div-->
                     </div>
                     <div class="sections price-total bg-yellow">                     
                        <div class="total">
                            <div>
                                <h4>printing total</h4>
                            </div>
                            <div>
                            </div>
                        </div>
                        <div class="total">
                            <div>
                                <h4>unit price</h4>
                            </div>
                            <div>
                            </div>
                        </div>                    
                    </div>
                    
                    <div class="btn-div text-center">
						<button type="submit" class="btn-blue btn-medium">place order</button>
                        <!--a class="btn-blue btn-small" href="#">print estimate</a-->
                    </div>
                    <!--div class="sections">  
                        <div class="estimate-shipping text-left">
                            <h5>estimate shipping</h5>
                        </div>
                        <div class="card-options">
                            <h6>zip / postal code</h6>
                            <input type="text" name="zip" id="zip" value="zip" />
                            <a class="btn-blue btn-small" href="#">get rates</a>
                        </div>
                    </div-->
                </div>
        
	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $wqp_product_id ); ?>" />
		<input type="hidden" name="product-id" id="product-id" value="" />
		<input type="hidden" name="quantity" value="1" />    
            </div>        
        
</form>
</div>