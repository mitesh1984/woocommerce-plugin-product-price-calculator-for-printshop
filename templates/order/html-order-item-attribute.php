<?php
global $item_detail;
if( !isset( $item_detail['product_id'] ) ){
	return;
}
$_custom_text = get_post_meta( $item_detail['product_id'], '_custom_text', true );

?>
<table class="order_item_details">
<?php
	$_wqp_attr = unserialize( $item_detail['wqp_attr'] );
	$_art = unserialize( $item_detail['art']);
	
	if( $_wqp_attr ){
		$has_product_attr = true;
		foreach( $_wqp_attr as $key => $attr ){
			if( $key != 'product-id' ){
				if( is_array( $attr ) ){
					foreach( $attr as $key => $text ){
						echo sprintf( '<tr><th>%s </th><td>%s</td></tr>', ucwords( $_custom_text[ $key ] ), $text );
					}	
				}else{
					if( $key == 'quantity' ){
						$attr = array_reverse( explode('-', $_wqp_attr['product-id'] ) )[0].' Units';
					}
					echo sprintf( '<tr><th>%s </th><td>%s</td></tr>', ucwords( str_replace( array('pa_','-'), array('', ' '), $key ) ), $attr );
				}
			}
		}
	}
	if( $_art ){
		foreach( $_art as $key => $value ){
			echo sprintf( '<tr><th>%s </th><td><a href="%s" class="button" target="_blank">Download</a></td></tr>', $key, $value );
		}
	}
?>
</table>