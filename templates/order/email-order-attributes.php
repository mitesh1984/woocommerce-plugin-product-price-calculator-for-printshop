<?php
global $order_detail;
$content = '';

foreach( $order_detail->get_items() as $item_id => $item ) {
	$_wqp_attr = wc_get_order_item_meta( $item_id, '_wqp_attr' );
	
	if( $_wqp_attr ){
		$content .= '
		<tr>
			<th class="td" style="text-align:left; vertical-align:top; border: 1px solid #eee; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;">
				'.$item['name'].'
			</th>
			<td class="td" style="text-align:left; vertical-align:middle; border: 1px solid #eee; font-family: \'Helvetica Neue\', Helvetica, Roboto, Arial, sans-serif;">';
		?>
		<?php
		foreach( $_wqp_attr as $key => $attr ){
			if( $key != 'product-id' ){
				if( is_array( $attr ) ){
					$_custom_text = get_post_meta( $item['product_id'], '_custom_text', true );
					foreach( $attr as $key => $text ){
						echo ucwords( $_custom_text[ $key ] ).': '.$text.'<br/>';
					}
				}else{
					if( $key == 'quantity' ){
						$attr = array_reverse( explode('-', $_wqp_attr['product-id'] ) )[0].' Units';
					}
					$content .= ucwords( str_replace( array('pa_','-'), array('', ' '), $key ) ).': '.$attr.'<br/>';
				}
			}
		}
		$content .= '
			</td>
		</tr>';
	}
}
if( $content ){
?>
	<h2>Product Detail</h2>
	<table style="width:100%;font-family:'Helvetica Neue',Helvetica,Roboto,Arial,sans-serif;color:#737373;border:1px solid #e4e4e4" width="100%" border="1" cellspacing="0" cellpadding="0">
<?php
echo $content;
?>
	</table>
<?php
}
?>