<?php
global $order_detail;
$has_product_attr = false;
$has_art = true;
$art_images =
	array('4/0'	=>	array(
						  'matter'	=>	array('Front Upload'),
						  'spot uv front' => array('Front Upload', 'Front UV Upload'),
						  'uv front'	=>	array('Front Upload'),
						  ),
		  '4/1'	=>	array(
						  'matter'	=>	array('Front Upload', 'Back Upload'),
						  'spot uv front'	=>	array('Front Upload', 'Back Upload', 'Front UV Upload'),
						  'spot uv both sides' => array('Front Upload', 'Back Upload', 'Front UV Upload', 'Back UV Upload'),
						  'uv front'	=>	array('Front Upload', 'Back Upload'),
						  'uv both sides'	=>	array('Front Upload', 'Back Upload'),
						  ),
		  '4/2'	=>	array(
						  'matter'	=>	array('Front Upload', 'Back Upload'),
						  'spot uv front'	=>	array('Front Upload', 'Back Upload', 'Front UV Upload'),
						  'spot uv both sides' => array('Front Upload', 'Back Upload', 'Front UV Upload', 'Back UV Upload'),
						  'uv front'	=>	array('Front Upload', 'Back Upload'),
						  'uv both sides'	=>	array('Front Upload', 'Back Upload'),
						  ),
		  );
//array('front-upload', 'front-uv-upload', 'back-upload', 'back-uv-upload');
ob_start();
?>
<form method="post" action="" enctype="multipart/form-data">
	<table class="order_details">
	<?php
	foreach( $order_detail->get_items() as $item_id => $item ) {
		$_wqp_attr = wc_get_order_item_meta( $item_id, '_wqp_attr' );
		$_wqp_art = wc_get_order_item_meta( $item_id, 'art' );
		
		if( $_wqp_attr ){
			$has_product_attr = true;
			?>
			<tr>
				<th>
					<?php echo $item['name']; ?>
				</th>
				<td>
			<?php
			$coating = $color = '';
			foreach( $_wqp_attr as $key => $attr ){
				if( $key != 'product-id' ){
					if( is_array( $attr ) ){
						$_custom_text = get_post_meta( $item['product_id'], '_custom_text', true );
						foreach( $attr as $key => $text ){
							echo ucwords( $_custom_text[ $key ] ).': '.$text.'<br/>';
						}
					}else{
						if( $key == 'quantity' && isset($_wqp_attr['product-id']) ){
							//$attr = array_reverse( explode('-', $_wqp_attr['product-id'] ) )[0].' Units';
						}
						if( $key == 'pa_color' ){
							$color = $attr;
						}
						if( $key == 'pa_coating' ){
							$coating = strtolower( $attr );
						}
						echo ucwords( str_replace( array('pa_','-'), array('', ' '), $key ) ).': '.$attr.'<br/>';
					}
				}
			}
			
				$color = '4/1';
				$coating = 'spot uv both sides';
			if( ! $_wqp_art ){
				$has_art = false;
				if ( $art_images[$color][$coating] ){
					echo '<strong>You have to upload art for complete order.</strong>';
					echo '<table>';
					foreach( $art_images[$color][$coating] as $imageName ){
						echo '<tr>
									<td>'.$imageName.'</td>
									<td><input type="file" name="art['.$item_id.']['.$imageName.']" /></td>
								</tr>';
					}
					echo '</table>';
				}
			}else{
				echo '<strong>Uploaded Art</strong>';
				
				if( is_array($_wqp_art) ){
					if ( $art_images[$color][$coating] ){
						echo '<table>';
						foreach( $art_images[$color][$coating] as $imageName ){
							echo '<tr>
										<td>'.$imageName.'</td>';
										if( isset($_wqp_art[$imageName]) ){
											echo	'<td><img src="'.$_wqp_art[$imageName].'" alt="" /></td>';
										}else{
											$has_art = false;
											echo '<td><input type="file" name="art['.$item_id.']['.$imageName.']" /></td>';
										}
							echo '</tr>';
						}
						echo '</table>';
					}
				}
			}
			//wc_delete_order_item_meta( $item_id, 'art' );
			?>
				</td>
			</tr>
			<?php
		}
		
	}
	?>
	</table>
	<?php if( !$has_art ){ ?>
	<input type="submit" name="upload_art" value="Upload Art" />
	<?php } ?>
</form>
<?php
$content = ob_get_contents();
ob_clean();

if( $has_product_attr ){
?>
	<h2>Product Detail</h2>
<?php
echo $content;
}
?>