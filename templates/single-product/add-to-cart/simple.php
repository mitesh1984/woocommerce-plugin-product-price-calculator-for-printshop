<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$attributes = $product->get_attributes();
$_unit_price = get_post_meta( $product->id, '_unit_price', true );
$_custom_text = get_post_meta( $product->id, '_custom_text', true );
$_units = get_post_meta( $product->id, '_units', true );

if ( ! $product->is_purchasable() ) {
	return;
}

$product_attr = WC()->session->get( 'wqp_product_attr' );
if( isset( $product_attr[ $product->id ] ) ){
	$product_attr = $product_attr[ $product->id ];
}

?>

<?php
	// Availability
	$availability      = $product->get_availability();
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
?>

<?php if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" method="post" enctype='multipart/form-data'>
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

<table class="bulk_product_panel">
	 	<?php
		if( $_custom_text ){
			foreach( $_custom_text as $key => $text ):
			?>
			<tr>
				<th><?php echo $text; ?></th>
				<td><input type="text" name="custom_text[<?php echo $key?>]" value="" /></td>
			</tr>
			<?php
			endforeach;
		}
		foreach ( $attributes as $attribute ) :
			if( $attribute['is_bulk'] ):
		?>
		<tr>
			<th><?php echo wc_attribute_label( $attribute['name'] ); ?></th>
			<td><?php
				if ( $attribute['is_taxonomy'] ) {
					$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'all' ) );
					?>
					<select name="<?php echo $attribute['name']?>">
					<?php
					foreach( $values as $value ){
						?>
						<option data-id="<?php echo $value->term_id;?>" <?php echo ( isset( $product_attr[ $attribute['name'] ] ) && $product_attr[ $attribute['name'] ] == $value->name ? 'selected' : '' )?> value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>
						<?php
					}
					?>
					</select>
					<?php
				} 
			?></td>
		</tr>
	<?php
			endif;
		endforeach;
		foreach ( $attributes as $attribute ) :
			if( !$attribute['is_bulk'] ):
		?>
		<tr>
			<th><?php echo wc_attribute_label( $attribute['name'] ); ?></th>
			<td><?php
				if ( $attribute['is_taxonomy'] ) {
					$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'all' ) );
					?>
					<select name="<?php echo $attribute['name']?>">
					<?php
					foreach( $values as $value ){
						?>
						<option data-id="<?php echo $attribute['name'];?>" <?php echo ( isset( $product_attr[ $attribute['name'] ] ) && $product_attr[ $attribute['name'] ] == $value->name ? 'selected' : '' )?> value="<?php echo $value->name; ?>"><?php echo $value->name; ?></option>
						<?php
					}
					?>
					</select>
					<?php
				} 
			?></td>
		</tr>
	<?php
			endif;
		endforeach;
	
	if( is_array( $_units ) && count( $_units ) > 0 ){
		
		foreach( $_unit_price as $key => $unit ){
			$_single_unit = array_reverse( explode( '-', $key ) )[0];
			$_unit_price[ $key ] = wc_price( $unit ).' / '. wc_price( $unit / $_single_unit ) .' Unit price' ;
		}
	?>
		<tr>
			<th>Units</th>
			<td>
				<select name="units" data-price='<?php echo json_encode( $_unit_price ); ?>'>
	<?php
		$product_units = 0;
		if( isset( $product_attr[ 'product-id' ] ) ) {
			$product_units = array_reverse( explode('-', $product_attr[ 'product-id' ] ) )[0];	
		}
		
		foreach( $_units as $key => $unit ){
			echo '<option data-id="'.$unit.'" '.( $product_units == $unit ? 'selected' : '' ).' value="'.$unit.'">'.$unit.'</option>';
		}
	 	?>
				</select>
			</td>
		</tr>
		<?php
		}
		?>
	</table>
		<?php wc_get_template( 'single-product/price.php' );?>
	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
		<input type="hidden" name="product-id" id="product-id" value="" />
		<input type="hidden" name="quantity" value="1" />
	 	<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
