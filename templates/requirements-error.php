<div class="error">
	<p><?php echo WQP_NAME; ?> error: Your environment doesn't meet all of the system requirements listed below.</p>

	<ul class="ul-disc">
		<?php
			echo  ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) ? '<li><strong>WooCommerce Plugin</strong> needs to be activate.</em></li>' : '';
		 ?>
	</ul>

	<p>If you need to upgrade your version of PHP you can ask your hosting company for assistance, and if you need help upgrading WordPress you can refer to <a href="http://codex.wordpress.org/Upgrading_WordPress">the Codex</a>.</p>
</div>
