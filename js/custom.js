(function($){

    $(window).load(function(){
        
        $(".select-div select").selectbox();
        
        if ( $('.bulk_product_panel').size() > 0 ) {
            getProductPrice();
        }
        
        $('.bulk_product_panel select').change(function(){
            getProductPrice();
        });
        
        $('select[name="pa_coating"]').change(function(){
            var val = $(this).val().toLowerCase();
            var form = $(this).parents('form');
            
            if( val == 'matte' || val == 'spot uv front' || val == 'uv front' ){
                $(form).find('select[name="pa_color"] option[value="4/0"]').removeAttr('disabled');
                $(form).find('select[name="pa_color"]').val('4/0');
            }else{
                $(form).find('select[name="pa_color"] option[value="4/0"]').attr('disabled','disabled');
                $(form).find('select[name="pa_color"]').val('4/1');
            }
        });

        
    });

    function getProductPrice() {        
        $('.bulk_product_panel').each(function(){
            var priceID = '';
            var seperator = '';
            var lastID = '';
            $(this).find('select').each(function(){                
                if ( $(this).find('option:selected').attr('data-id') !== undefined &&
                    $(this).find('option:selected').attr('data-id') !== false ) {
                    priceID =  priceID + seperator + $(this).find('option:selected').attr('data-id');
                    seperator = '-';
                    lastID = $(this).find('option:selected').attr('data-id');
                    
                }                
            });
            if ( $( this ).hasClass('price-calculator') ) {
                $(this).find('.price-total').html( $(this).find('select[name="units"]').data('price')[ priceID ] );
                $(this).find('#product-id').val( priceID );
            }else{
                $('.single-product .cart .price').html( $(this).find('select[name="units"]').data('price')[ priceID ] );
                $('#product-id').val( priceID );
            } 
        });
    }
})(jQuery);