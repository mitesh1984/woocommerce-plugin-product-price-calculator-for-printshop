(function($){

    $(document).ready(function(){
       // Show/add price fields for bulk products  
       $('.product_unit_add').click(function(){
            $('.bulk_product_handler_panel').append('<div class="unit_price_panel">'
                                                    + '<input type="text" class="product_units" name="product_units[]" value="" />'
                                                    + '<input type="button" class="product_unit_remove button" value="Remove" />'
                                                    + '</div>');
        });
       //For latest jquery version
       $('.product_unit_remove').on('click', '.bulk_product_handler_panel', function(){
            if ( confirm('Are you sure?') ) {
                $(this).parent('.unit_price_panel').remove();
            }
        });
       //For lowest jquery version
       $('.product_unit_remove').live('click', function(){
            if ( confirm('Are you sure?') ) {
                $(this).parent('.unit_price_panel').remove();
            }
        });
       
       
       // Show/add price fields for bulk products  
       $('.product_custom_text_add').click(function(){
            $('.bulk_product_text_panel').append('<div class="unit_text_panel">'
                                                    + '<input type="text" class="product_texts" name="product_custom_text[]" placeholder="Add lable for input text" value="" />'
                                                    + '<input type="button" class="product_text_remove button" value="Remove" />'
                                                    + '</div>');
        });
       //For latest jquery version
       $('.product_text_remove').on('click', '.bulk_product_text_panel', function(){
            if ( confirm('Are you sure?') ) {
                $(this).parent('.unit_text_panel').remove();
            }
        });
       //For lowest jquery version
       $('.product_text_remove').live('click', function(){
            if ( confirm('Are you sure?') ) {
                $(this).parent('.unit_text_panel').remove();
            }
        });
       
    });

})(jQuery);
