<?php

// Generate combination base on data
function _wqp_combination($data, &$all = array(), $group = array(), $val = null, $i = 0)
{
	if (isset($val))
	{
		array_push($group, $val);
	}

	if ($i >= count($data))
	{
		array_push($all, $group);
	}
	else
	{
		foreach ($data[$i] as $v)
		{
			_wqp_combination( $data, $all, $group, $v, $i + 1 );
		}
	}

	return $all;
}

