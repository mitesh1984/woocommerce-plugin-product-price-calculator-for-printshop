<?php
/*
Plugin Name: WooCommerce Quatity base Price
Plugin URI: http://www.prismitsystems.com/
Version: 1.0.0
Description: Add quantiy base price in individual product
Author URI: http://www.prismitsystems.com/
*/

define( 'WQP_NAME','WooCommerce Quatity Base Price' );
define( 'WQP_PATH', plugin_dir_path(__FILE__) );
define( 'WQP_URL', plugin_dir_url(__FILE__) );

if( !function_exists('wqp_requirements_check') ){
	function wqp_requirements_check() {
		global $wp_version;
		
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );		// to get is_plugin_active() early
	
		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			return false;
		}
		
		return true;
	}
}

/**
 * Prints an error that the system requirements weren't met.
 */
function wqp_requirements_error() {
	global $wp_version;
	require_once( dirname( __FILE__ ) . '/templates/requirements-error.php' );
}


/*
 * Check requirements and load main class
 * The main program needs to be in a separate file that only gets loaded if the plugin requirements are met. Otherwise older PHP installations could crash when trying to parse it.
 */
if ( wqp_requirements_check() ) {
	
	register_activation_hook( __FILE__, function(){
		update_option( '_wqp_imports', 0 );	
	} );
	
	require_once("include/functions.php");
	require_once("class/class-wqp.php");
	require_once("class/class-bulk-product-metabox.php");

} else {
	deactivate_plugins( plugin_basename( __FILE__ ) );
	add_action( 'admin_notices', 'wqp_requirements_error' );
}
